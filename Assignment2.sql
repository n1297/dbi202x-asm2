/*Type of Triangle*/
/*
Write a query identifying the type of each record in the TRIANGLES table using its three side lengths. 
Output one of the following statements for each record in the table:

Equilateral: It's a triangle with 3 sides of equal length.
Isosceles: It's a triangle with 2 sides of equal length.
Scalene: It's a triangle with 3 sides of differing lengths.
Not A Triangle: The given values of A, B, and C don't form a triangle.*/

SELECT (CASE             
            WHEN A + B > C AND B + C > A AND A + C > B THEN
                (CASE 
                    WHEN A = B AND B = C THEN 'Equilateral'
                    WHEN A = B OR B = C OR A = C THEN 'Isosceles'
                    ELSE 'Scalene'
                END)
            ELSE 'Not A Triangle'
        END)
FROM TRIANGLES;

/*The PADS*/
/*
Generate the following two result sets:

Query an alphabetically ordered list of all names in OCCUPATIONS, 
immediately followed by the first letter of each profession as a parenthetical (i.e.: enclosed in parentheses). 
For example: AnActorName(A), ADoctorName(D), AProfessorName(P), and ASingerName(S).
Query the number of ocurrences of each occupation in OCCUPATIONS. 
Sort the occurrences in ascending order, and output them in the following format:

There are a total of [occupation_count] [occupation]s.
where [occupation_count] is the number of occurrences of an occupation in OCCUPATIONS and [occupation] is the lowercase occupation name. 
If more than one Occupation has the same [occupation_count], they should be ordered alphabetically.

Note: There will be at least two entries in the table for each type of occupation.
*/

/*Querry 1*/

SELECT CONCAT(Name, '(' , LEFT(Occupation,1), ')') 
FROM OCCUPATIONS 
ORDER BY Name ASC;

/*Querry 2*/

SELECT CONCAT('There are a total of ', COUNT(Occupation), ' ', LOWER(Occupation), 's.') 
FROM OCCUPATIONS 
GROUP BY Occupation 
ORDER BY COUNT(Occupation), Occupation ASC;

/*Occupations*/
/*
Pivot the Occupation column in OCCUPATIONS so that each Name is sorted alphabetically and displayed underneath its corresponding Occupation. The output column headers should be Doctor, Professor, Singer, and Actor, respectively.

Note: Print NULL when there are no more names corresponding to an occupation.
*/

SELECT
    [Doctor], [Professor], [Singer], [Actor]
FROM
(
    SELECT ROW_NUMBER() OVER (PARTITION BY OCCUPATION ORDER BY NAME) [RowNumber], * FROM OCCUPATIONS
) AS tempTable
PIVOT
(
    MAX(NAME) FOR OCCUPATION IN ([Doctor], [Professor], [Singer], [Actor])
) AS pivotTable

/*Revising Aggregations - The Sum Function*/
/*
Query the total population of all cities in CITY where District is California.
*/

SELECT SUM(POPULATION) 
FROM CITY
WHERE DISTRICT ='California';

/*Revising Aggregations - The Count Function*/
/*
Query a count of the number of cities in CITY having a Population larger than 100000 .
*/

SELECT COUNT(*) 
FROM CITY
WHERE POPULATION > 100000;

/*Revising Aggregations - Averages*/
/*
Query the average population of all cities in CITY where District is California.
*/

SELECT AVG(POPULATION) 
FROM CITY
WHERE DISTRICT ='California';

/*Average Population*/
/*
Query the average population of all cities in CITY where District is California.
*/

SELECT ROUND(AVG(POPULATION),0) 
FROM CITY;

/*Japan Population*/
/*
Query the sum of the populations for all Japanese cities in CITY. The COUNTRYCODE for Japan is JPN.
*/

SELECT SUM(POPULATION) 
FROM CITY
WHERE COUNTRYCODE ='JPN';

/*Population Density Difference*/
/*
Query the difference between the maximum and minimum populations in CITY.
*/

SELECT (MAX(POPULATION)-MIN(POPULATION)) 
FROM CITY;

/*The Blunder*/
/*
Samantha was tasked with calculating the average monthly salaries for all employees in the EMPLOYEES table, 
but did not realize her keyboard's 0 key was broken until after completing the calculation. 
She wants your help finding the difference between her miscalculation (using salaries with any zeros removed), 
and the actual average salary.

Write a query calculating the amount of error (i.e.:  average monthly salaries), and round it up to the next integer.
*/

SELECT CAST(CEILING((AVG(CAST(Salary AS FLOAT)) - AVG(CAST(REPLACE(Salary, 0, '')AS FLOAT)))) AS INT) 
FROM EMPLOYEES;

/*Weather Observation Station 2*/
/*
Query the following two values from the STATION table:

The sum of all values in LAT_N rounded to a scale of  decimal places.
The sum of all values in LONG_W rounded to a scale of  decimal places.
*/

/*Cách 1*/
/*Ghi vào để khỏi quên, tìm hiểu FORMAT sau*/

SELECT FORMAT(ROUND(SUM(LAT_N),2),'#.00'),
FORMAT(ROUND(SUM(LONG_W),2),'#.00')
FROM STATION;

/*Cách 2*/

SELECT CAST(SUM(LAT_N) AS DECIMAL(10,2)) , CAST(SUM(LONG_W) AS DECIMAL(10,2)) 
FROM STATION; 

/*Weather Observation Station 13*/
/*
Query the sum of Northern Latitudes (LAT_N) from STATION having values greater than 38.7780 and less than 137.2345 . 
Truncate your answer to 4 decimal places.
*/

SELECT CAST(SUM(LAT_N)AS DECIMAL(10,4)) 
FROM STATION
WHERE LAT_N > 38.7880 AND LAT_N < 137.2345;

/*Weather Observation Station 14*/
/*
Query the greatest value of the Northern Latitudes (LAT_N) from STATION that is less than 137.2345. 
Truncate your answer to 4 decimal places.
*/

SELECT CAST(MAX(LAT_N) AS DECIMAL(10,4))
FROM STATION
WHERE LAT_N < 137.2345;

/*Weather Observation Station 16*/
/*
Query the smallest Northern Latitude (LAT_N) from STATION that is greater than 38.7780. 
Round your answer to 4 decimal places.
*/

SELECT CAST(MIN(LAT_N) AS DECIMAL(10,4))
FROM STATION
WHERE LAT_N > 38.7780;

/*Weather Observation Station 18*/

/*
https://www.hackerrank.com/challenges/weather-observation-station-18/problem

Consider P1(a,b) and P2(c,d) to be two points on a 2D plane.

 a happens to equal the minimum value in Northern Latitude (LAT_N in STATION).
 b happens to equal the minimum value in Western Longitude (LONG_W in STATION).
 c happens to equal the maximum value in Northern Latitude (LAT_N in STATION).
 d happens to equal the maximum value in Western Longitude (LONG_W in STATION).
Query the Manhattan Distance between points  and  and round it to a scale of  decimal places.
*/

SELECT CAST(((MAX(LAT_N)-MIN(LAT_N))+(MAX(LONG_W)-MIN(LONG_W))) AS DECIMAL(10,4)) 
FROM STATION;

/*Weather Observation Station 19*/
/*
Consider P1(a,c) and P2(b,d) to be two points on a 2D plane 
where (a,b) are the respective minimum and maximum values of Northern Latitude (LAT_N) and 
(c,d) are the respective minimum and maximum values of Western Longitude (LONG_W) in STATION.

Query the Euclidean Distance between points  and  and format your answer to display  decimal digits.
*/

SELECT CAST(SQRT((POWER(MAX(LAT_N)  - MIN(LAT_N),2)) +(POWER(MAX(LONG_W) - MIN(LONG_W),2)))AS DECIMAL(10,4)) 
FROM STATION;

/*Top Competitors*/
/*
Julia just finished conducting a coding contest,
 and she needs your help assembling the leaderboard! 
 Write a query to print the respective hacker_id and name of hackers who achieved full scores for more than one challenge. 
 Order your output in descending order by the total number of challenges in which the hacker earned a full score. 
 If more than one hacker received full scores in same number of challenges, then sort them by ascending hacker_id.
 */

SELECT h.hacker_id, h.name
FROM
    hackers h
    INNER JOIN
    (
        SELECT s.hacker_id, COUNT(s.hacker_id) AS cnt
        FROM
            challenges c
            INNER JOIN difficulty d
                on c.difficulty_level = d.difficulty_level
            INNER JOIN submissions s
                on c.challenge_id = s.challenge_id
        WHERE
            s.score = d.score
        GROUP BY  s.hacker_id
        HAVING COUNT(s.hacker_id) > 1
    ) AS ch
        ON h.hacker_id = ch.hacker_id
ORDER BY
    ch.cnt DESC,
    h.hacker_id

/*Placements*/
/*
You are given three tables: Students, Friends and Packages. 
Students contains two columns: ID and Name. 
Friends contains two columns: ID and Friend_ID (ID of the ONLY best friend). 
Packages contains two columns: ID and Salary (offered salary in $ thousands per month).

Write a query to output the names of those students whose best friends got offered a higher salary than them. 
Names must be ordered by the salary amount offered to the best friends. 
It is guaranteed that no two students got same salary offer.
*/

SELECT S.Name
FROM Students S
    JOIN Friends F ON S.ID = F.ID
    JOIN Packages P ON S.ID = P.ID
    JOIN Packages P2 ON F.Friend_ID = P2.ID
WHERE P.Salary < P2.Salary
ORDER BY P2.Salary